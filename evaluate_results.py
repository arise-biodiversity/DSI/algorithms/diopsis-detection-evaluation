import argparse
import glob
import json
import os
from typing import List

import matplotlib.pyplot as plt
import numpy as np
import pandas

from object_detection_metrics.bounding_box import BoundingBox
from object_detection_metrics.evaluators import pascal_voc_evaluator
from object_detection_metrics.utils.enumerators import (
    BBFormat,
    MethodAveragePrecision,
    BBType,
)

metric_weighting_factors = {
    "f1_max": 1.0,
    "recall_max": 0.50,
    "precision_max": 0.50,
    "ap": 1.0,
    "0_25_f1_max": 2.00,
    "0_25_recall_max": 1.00,
    "0_25_precision_max": 1.00,
    "0_25_ap": 2.00,
    "25_50_f1_max": 1.00,
    "25_50_recall_max": 0.5,
    "25_50_precision_max": 0.5,
    "25_50_ap": 1.0,
    "50_75_f1_max": 0.5,
    "50_75_recall_max": 0.25,
    "50_75_precision_max": 0.25,
    "50_75_ap": 0.50,
    "75_100_f1_max": 0.25,
    "75_100_recall_max": 0.125,
    "75_100_precision_max": 0.125,
    "75_100_ap": 0.25,
}


def get_statistic_by_name(out_stats, name) -> float:
    return out_stats[out_stats.statistic == name].value.values[0]


def f1_func(precision, recall):
    return 2 * np.nan_to_num((precision * recall) / (precision + recall))


def evaluate_folder(
    true_folder: str, predicted_folder: str, out_statistics_path: str
) -> pandas.DataFrame:

    img_size = (3280, 2464)

    true_boxes = read_folder(true_folder, img_size)
    predicted_boxes = read_folder(predicted_folder, img_size)

    return evaluate(true_boxes, predicted_boxes, out_statistics_path)


def evaluate(
    true_boxes: List[BoundingBox],
    predicted_boxes: List[BoundingBox],
    out_statistics_path: str,
    plot=False,
) -> pandas.DataFrame:

    img_size = (3280, 2464)
    img_area = img_size[0] * img_size[1]

    areas = [box_.get_area() / img_area for box_ in true_boxes]

    percentiles = np.arange(0, 125, 25)

    thresholds = [np.percentile(areas, p_) for p_ in percentiles]

    #
    iou = 0.8
    voc_res = pascal_voc_evaluator.get_pascalvoc_metrics(
        true_boxes,
        predicted_boxes,
        iou,
        generate_table=False,
        method=MethodAveragePrecision.EVERY_POINT_INTERPOLATION,
    )
    f1, precision_max, recall_max = compute_max(voc_res)
    # for k, v in voc_res["per_class"]["Object"].items():
    #     print(k, v)
    out_stats_table = []
    out_stats_table.append(
        (
            f"f1_max",
            f1.max(),
        )
    )
    out_stats_table.append(
        (
            f"recall_max",
            recall_max.max(),
        )
    )
    out_stats_table.append(
        (
            f"precision_max",
            precision_max.max(),
        )
    )
    out_stats_table.append(
        (
            f"ap",
            voc_res["per_class"]["Object"]["AP"],
        )
    )

    f1s = []
    recalls = []
    precisions = []
    aps = []
    for a, min_area in enumerate(thresholds[:-1]):
        true_boxes_min = filter_boxes_by_area(
            min_area, thresholds[a + 1], true_boxes, img_area
        )
        print("|true_boxes_min|", len(true_boxes_min))
        print("|predicted_boxes|", len(predicted_boxes))

        voc_res = pascal_voc_evaluator.get_pascalvoc_metrics(
            true_boxes_min,
            predicted_boxes,
            iou,
            generate_table=False,
            method=MethodAveragePrecision.EVERY_POINT_INTERPOLATION,
        )

        res = voc_res["per_class"]["Object"]
        print(res["AP"])
        precision = res["precision"]
        recall = res["recall"]
        f1 = f1_func(
            precision,
            recall,
        )
        f1_max, precision_max, recall_max = compute_max(voc_res)

        f1s.append(f1_max)
        recalls.append(recall_max)
        precisions.append(precision_max)
        ap = res["AP"]
        aps.append(ap)

        out_stats_table.append(
            (
                f"{percentiles[a]}_{percentiles[a+1]}_f1_max",
                f1.max(),
            )
        )
        out_stats_table.append(
            (
                f"{percentiles[a]}_{percentiles[a+1]}_recall_max",
                recall_max.max(),
            )
        )
        out_stats_table.append(
            (
                f"{percentiles[a]}_{percentiles[a+1]}_precision_max",
                precision_max.max(),
            )
        )
        out_stats_table.append(
            (
                f"{percentiles[a]}_{percentiles[a+1]}_ap",
                ap,
            )
        )

    if plot:
        thresholds = np.array(thresholds)
        thresholds = (thresholds[:-1] + thresholds[1:]) / 2
        plt.semilogx(thresholds * 100, np.array(f1s) * 100, label="F-score")
        plt.semilogx(thresholds * 100, np.array(recalls) * 100, label="Recall")
        plt.semilogx(thresholds * 100, np.array(precisions) * 100, label="Precision")
        plt.semilogx(thresholds * 100, np.array(aps) * 100, label="AP")
        plt.ylim(0, 101)
        plt.xlabel("Minimal insect area (% of image)")
        plt.ylabel("Detection performance (%)")
        plt.legend(loc="lower right")

        plt.tight_layout()
        plt.show()

    #

    # compute one metric for the leaderboard
    overall_metric = 0  # TODO

    out_stats = pandas.DataFrame(data=out_stats_table, columns=["statistic", "value"])

    for _, row in out_stats.iterrows():
        overall_metric += (
            get_statistic_by_name(out_stats, row.statistic)
            * metric_weighting_factors[row.statistic]
        )
    overall_metric /= sum(metric_weighting_factors.values())
    out_stats.loc[len(out_stats.index)] = ["overall_metric", overall_metric]

    #
    out_stats["value"] = out_stats["value"].round(4)
    out_stats.to_csv(out_statistics_path, index=False)

    for _, row in out_stats.iterrows():
        print(row.values)

    return out_stats


def read_folder(true_folder, img_size):
    true_boxes: List[BoundingBox] = []
    for path in glob.glob(f"{true_folder}/*.json"):
        with open(path, "r") as f:
            j_ann = json.load(f)
        for annotation in j_ann["annotations"]:
            true_boxes.append(
                BoundingBox(
                    image_name=os.path.splitext(os.path.basename(path))[0],
                    class_id=annotation["labels"][0]["name"],
                    coordinates=(
                        annotation["shape"]["x"],
                        annotation["shape"]["y"],
                        annotation["shape"]["width"],
                        annotation["shape"]["height"],
                    ),
                    confidence=annotation["labels"][0]["probability"],
                    format=BBFormat.XYWH,
                    bb_type=BBType.GROUND_TRUTH,
                    img_size=img_size,
                )
            )
    return true_boxes


def compute_max(voc_res):
    precision = voc_res["per_class"]["Object"]["precision"]
    recall = voc_res["per_class"]["Object"]["recall"]
    f1 = f1_func(
        precision,
        recall,
    )
    f1_max_index = np.argmax(f1)
    recall_max = recall[f1_max_index]
    precision_max = precision[f1_max_index]
    return f1.max(), precision_max, recall_max


def filter_boxes_by_area(min_area, max_area, true_boxes: List[BoundingBox], img_area):
    selection = []
    for box in true_boxes:
        if min_area < (box.get_area() / img_area) <= max_area:
            selection.append(box)
    return selection


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Computes statistics for the detection task of the DIOPSIS challenge. See "
        "http://docs.google.com/document/d/1LBPz2MkcgNyzUZ729veDz3XgjhpkL1hMeBDFhfunfXY"
        " for explanation of the challenge and file formats",
        epilog="Example usage:"
        "python evaluate_results.py /path/to/true_folder /path/to/predicted_folder out_stats.csv",
    )
    parser.set_defaults(func=lambda x: parser.print_usage())
    parser.add_argument("true_folder", help="Folder containing truth JSON")
    parser.add_argument("predicted_folder", help="Folder containing predicted JSON")
    parser.add_argument(
        "out_path", help="Output file where performance statistics are written to"
    )
    args = parser.parse_args()
    evaluate_folder(
        true_folder=args.true_folder,
        predicted_folder=args.predicted_folder,
        out_statistics_path=args.out_path,
    )
