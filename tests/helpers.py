import numpy as np


def generate_test(img_area, thresholds, true_boxes):
    pred_boxes = []
    pick_probs = np.array([0.3, 0.5, 0.6, 0.9])
    for true_box in true_boxes:
        t_ = len(pick_probs) - 1
        for t_ in range(len(thresholds) - 1):
            if thresholds[t_] < (true_box.get_area() / img_area) < thresholds[t_ + 1]:
                break
        if np.random.rand() < pick_probs[t_]:
            pred_boxes.append(true_box)
    return pred_boxes
