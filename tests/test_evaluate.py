import unittest
from evaluate_results import evaluate, read_folder
from tests.helpers import generate_test
import numpy as np

# REPLACE BY UNZIPPED CONTENTS OF https://drive.google.com/file/d/1EqgSRkjtUMnUEGAOAX6onKWeO0Ax51G6/view?usp=sharing
public_annotations_folder = (
    "/mnt/big/geti/projects/diopsis_detection_challenge/public/annotations"
)


class TestEvaluate(unittest.TestCase):
    def test_evaluate(self):
        img_size = (3280, 2464)
        true_boxes = read_folder(
            public_annotations_folder,
            img_size,
        )
        img_area = img_size[0] * img_size[1]
        areas = [box_.get_area() / img_area for box_ in true_boxes]

        percentiles = np.arange(0, 125, 25)

        thresholds = [np.percentile(areas, p_) for p_ in percentiles]
        out_stats = evaluate(
            true_boxes,
            generate_test(img_area, thresholds, true_boxes),
            "out_stats.csv",
        )
        print(out_stats)
